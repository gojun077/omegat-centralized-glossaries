12지 동물	the twelve legendary animals
「불가사의한 조선민화」	Mysterious Joseon Minhwa
『李朝の民畫』	The Folk Painting of Yi Dynasty Korea
가장 한국적	quintisentially Korean
가중나무	ailanthus tree
각저총	Gakjeochong (proper noun)
간결	concision	簡潔
겨레그림	gyeoregeurim, pictures of the people
고구려 고분벽화	ancient Goguryeo tomb murals
고단샤	Kodansha	講談社
고려시대	Goryeo Dynasty
고려청자	Goryeo celadon
관官	ruling class (royalty)	(궁宮)
관료적 귀족국가	bureauceatic aristocratic state
구애받지 않는 자유로움	untroubled freedom
국조시조신화	national progenitor myth	國祖始祖神話
군자국	land of gentlemen	君子國
궁중장식화	decorative royal court painting/s
궁중회화	the art of the royal court
기념품	souvenir items
길상	auspicious omens
김덕하	Kim Deok-ha
김두량	Kim Du-ryang
김득신	Kim Deuk-sin
김시	Kim Si
김식	Kim Sik
김유신	Kim Yu-shin
김호연	Kim Ho-yeon
김홍도	Kim Hong-do
까치호랑이	magpie tiger (tiger is sometime leopard depending on the depiction)
낙관	artist's signature seal
남계우	Nam Gye-u
도노무라 키치노스케	Kichinosuke Tonomura	外村吉之介
도상학	iconography
독자성	originality
동음	homophony	同音
두루미	red-crowned cranes
말다래	saddle flap
매산리	Maesan-ri
명대	Ming Dynasty
모란	peonies
모티프	motif
무관의 관복	official uniforms of military officers
무나카타 시코	棟方志功 Munakata Shiko
무속화	shamanic paintings
무용총	Muyongchong (proper noun)
문방도	文房圖 The Scholars Accouterments
문인	literary scholars
문인 계층	class of literary scholars	文人
문인화	scholarly paintings
미야자키 노리코	Miyazaki Noriko	宮崎法子
민예	minye / 'folk crafts.'
민예운동	folk crafts movement
민족주의	nationalist thought
민족주의적 이데올로기	nationalist ideology
민화	minhwa, folk painting
민화 붐	minhwa boom
민화의 열풍	'minhwa fever'
민화작가	folk painters
바보스럽다	dim-witted
반구대	Bangudae (proper noun)
반구대 암각화	Bangudae Petroglyphs (proper noun)	岩刻畵
백호	white tiger
벽화	mural(s)
변상벽	Byeon Sang-byeok
병풍그림	folding screen paintings
보고	repository	寶庫
부자	father and son
부적	shamanic paper talisman
부조	stone tablet
불합리성	irrationality
불화	Buddhist paintings
비유	比喩 metaphor
사군자	the four gracious plants (plum, orchid, chrysanthemum and bamboo)	四君子
사대부계층	noble class
사대부화가	noblemen painters
사신	the godly animals of the four compass points	四神
사신도	sashindo / 'four gods' paintings	四神圖
사찰벽화	temple murals
산수 (평사낙안)	平沙落雁 Landscape (Pyeongsanakan)
산신도	Sanshindo (proper noun)
삼청	the samcheong plants (pine, plum and bamboo)	三淸(소나무․매화․대나무)
상서	propitious omen	祥瑞
생활화	everyday paintings
서민문화	culture for ordinary people
서민의 일상과 통속	popular pursuits
서민회화	paintings for the masses
서상	瑞祥 lucky omens
서쪽 방위를 담당하는 수호신	the guardian spirit in charge of the western compass point
서화천기론	Seohwacheongiron	書畵賤技論 서화를 천한 기술로 여김
선비들	gentleman scholars
선사시대 청동기	prehistoric bronze ware
세리자와 케이스케	Keisuke Serizawa	芹沢銈介
세한삼우歲寒三友	Sehansamu (proper noun)
소박한	humble
속화	俗畵 sokhwa / commonplace painting
수묵화	black ink painting
시정의 속인	laymen of the administration
신경질적인	temperamental
신분 차별	status discrimination
신선	sacred mountain hermit
신성화	sanctified
신윤복	Sin Yun-bok
실학	silhak
심사정	Sim Sa-jeong
십이지신상군	twelve guardian animals
안경회	looking glass pictures	眼鏡絵
안악3호분	Anak Tomb No. 3 (proper noun)
암각화	petroglyphs
애정행각	displays of affection
야나기 무네요시	柳宗悦 Yanagi Muneyoshi
양면성	double-sided character
에마	ema wooden tokens	絵馬
여기	pastime	餘技
영모	animal fur	翎毛
영모화	yeongmohwa / animal painting / bird feathers and animal fur painting
예술 천시사상	aesthetic consciousness
오츠에	otsue	"Otsu-e" literally means pictures of the city Otsu. Otsu is located next to Kyoto 大津絵
왕건릉	the tomb of Wang Geon
왕릉	royal tomb
우아한	graceful
울주	Ulju County
원앙	mandarin duck
유흥문화	entertainment culture
윤덕희	Yun Deok-hui
윤두서	Yun Du-seo
읍하	deep bow	揖-- 읍하다; 두 손을 맞잡아 얼굴 앞으로 들어 올리고 허리를 앞으로 공손히 구부렸다가 몸을 펴면서 손을 내리다. 인사하는 예(禮)의 하나이다
이경윤	Yi Gyeong-yun
이규경	李圭景 Yi Gyu Gyeong
이암	Yi Am
이영윤	Yi Yeong-yun
이우환	Lee U-hwan	李禹煥
이조도자	ijodoja (Japanese), or 'Joseon pottery'
이징	Yi Jing
이타미 준	Itame Jun	伊丹潤
일과 놀이	work and recreation
일본 민예관	the Japan Folk Crafts Museum
일제강점기	Japanese colonial period
자유 분방한	unrestrained
장승업	Jang Seung-eop
장한종	Jang Han-jong
재현	reproduction (recording)	기록하다
정선	Jeong Seon	鄭敾 조선 후기의 화가(1676~1759). 국내의 명승고적을 찾아다니면서 진경적(眞景的)인 사생화를 많이 그려 한국적 산수화풍을 세웠다.
정홍래	Jeong Hong-rae
조선시대	Joseon Dynasty
조영석	Jo Yeong-seok
조자용	Jo Ja-yong
종교화	devotional paintings
종교화	religious painting
주류	mainstream
주작	vermillion bird
중국회화	Chinese painting
지령	command (wishing)	기원하다
책색화	chaesaekhwa / polychrome painting
천기	pastimes	賤技
천마도	Cheonmado (proper noun)	天馬圖
청룡	blue dragon
최북	Choi Buk
칠기	lacquerware
침체	stagnation
타락상	unscrupulous behaviours
통일신라시대	Unified Silla Dynasty
투박한	unrefined
파초	plantain
표출	expression (congratulating)	축하하다
풍속화	genre paintings
풍토성	endimicity
한국의 채색화	Chaesaekhwa Polychrome Painting of Korea
한민화	hanminhwa, minhwa of the'Han' (Korea)	韓民畵
한화	漢画 Han Chinese painting
해애	Hae Ae	海涯
현무	black tortoise
호담국	hodamguk,land of gentlemen
호랑이 석상	stone tigers
호렵도	Horyeopdo (proper noun)
호피도	Hopido (proper noun)
화원	official court painters
화조 · 산수화를 해독하다 - 중국 미술의 의미	Deciphering Hwajohwa and Landscape Painting: the Meanings of Chinese Art
화조화	hwajohwa / bird and flower painting
